Developer Toolbar installation
==============================

Source:  [link](http://www.mgt-commerce.com/magento-developer-toolbar.html)
Example: [link](http://developertoolbar.mgt-commerce.com/) 

Example of magento instance directory (before module installation):

```bash
- magento-project
  - htdocs (magento files)
  - composer.json
```
composer.json
------
```bash
 {
  "minimum-stability":"dev",
  "require":{
    "cm1/developer-toolbar": "*"
  },
  "repositories":[
    {
      "type":"vcs",
      "url":"https://vkubrak@bitbucket.org/gorilla-cm1/developer-toolbar.git"
    },
    {
       "type":"vcs",
       "url":"https://github.com/magento-hackathon/magento-composer-installer"
    }
  ],
  "extra":{
    "magento-root-dir":"./htdocs"
  }
}
```
In case you prefer copies to symlinks, no problem: simply set the appropriate config option under extra.magento-deploystrategy:

```bash
{
  ...
  "extra":{
    "magento-root-dir":"./htdocs",
    "magento-deploystrategy":"copy"
  }
}
```

Run composer installation:

```bash
cd /var/www/magento-project
composer install
```